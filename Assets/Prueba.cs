﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prueba : MonoBehaviour {

	private Shader shader1;
	private Shader shader2;
	private Shader shader3;
	private Shader shader4;
	private Renderer rend;
	public float contador = 0;
	public Text informacion;


	// Use this for initialization
	void Start () {

		rend = GetComponent<Renderer>();
		shader1 = Shader.Find("Custom/DiffuseLightExam");
		shader2 = Shader.Find("Custom/ScrollExam");
		shader3 = Shader.Find("Custom/NormalMapExam");
		shader4 = Shader.Find("Custom/NormalMapTextureExam");

	}
	
	// Update is called once per frame
	void Update () {

		contador += Time.deltaTime;
		if (contador >= 20)
			rend.material.shader = shader1;
		if (contador >= 30)
			rend.material.shader = shader2;
		if (contador >= 40)
			rend.material.shader = shader3;
		if (contador >= 50)
			rend.material.shader = shader4;
	}

	void Display () {

		informacion.text = "Shader:" + shader1.name;
	}
}
