﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transpose : MonoBehaviour {

	//public Vector3 targerPos;
	[SerializeField]
	private Transform originalTransform;
	[SerializeField]
	private Transform targetTransform;

	void Start(){

		transform.position = originalTransform.position;
	}

	// Update is called once per frame
	void Update () {
		//transform.Translate (Vector3.forward * Time.deltaTime);
		//transform.position = Vector3.Lerp(transform.position, targerPos, Time.deltaTime);
		//transform.position = new Vector3(100, 0, 200);
		//transform.position = Vector3.Lerp(originalTransform.position, targetTransform.position, Time.deltaTime);
		//transform.rotation = Quaternion.Lerp(originalTransform.rotation, targetTransform.rotation, Time.deltaTime);
	}
	IEnumerator CustomUpdate(){
		float originalx = transform.position.x;
		while (transform.position.x <= originalx + 5) 
		{
			transform.position = new Vector3 (transform.position.x + Time.deltaTime, transform.position.y, transform.position.z);
			yield return new WaitForEndOfFrame ();
		}
	}
}
