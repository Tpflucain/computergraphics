﻿Shader "Custom/NormalMapTextureExam" {
	Properties {
		_MainTex2 ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap2 ("Normal map", 2D) = "bump" {}
		_NormalMapIntensity2("Normal Intensity", Range (0,1)) = 0.5
		_ScrollXSpeed2("X scroll speed", Range(0,10)) = 2
		_ScrollYSpeed2("Y scroll speed", Range(0,10)) = 0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex2;
		sampler2D _NormalMap2;
		float _NormalMapIntensity2;
		fixed _ScrollXSpeed2;
		fixed _ScrollYSpeed2;


		struct Input {
			float2 uv_MainTex2;
			float2 uv_NormalMap2;

		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed2 scrolledUV = IN.uv_NormalMap2;
			fixed xScrollValue = _ScrollXSpeed2 * _Time;
			fixed yScrollValue = _ScrollYSpeed2 * _Time;
			scrolledUV += fixed2(xScrollValue, yScrollValue);
			fixed2 scrolledUV2 = IN.uv_MainTex2;
			fixed xScrollValue2 = _ScrollXSpeed2 * _Time;
			fixed yScrollValue2 = _ScrollYSpeed2 * _Time;
			scrolledUV2 += fixed2(xScrollValue2, yScrollValue2);
			o.Albedo =tex2D(_MainTex2, scrolledUV2).rgb;
			float3 normalMap = UnpackNormal(tex2D(_NormalMap2, scrolledUV));
			normalMap.x *= _NormalMapIntensity2;
			normalMap.y *= _NormalMapIntensity2;
			o.Normal = normalize(normalMap);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
