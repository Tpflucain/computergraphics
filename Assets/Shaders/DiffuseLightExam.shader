﻿Shader "Custom/DiffuseLightExam" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_AmbientColor ("AmbientColor", Color) = (1,1,1,1)
		_MySlider ("Slider", Range(0,5)) = 0.0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		fixed4 _AmbientColor;
		float _MySlider;

		void surf (Input IN, inout SurfaceOutputStandard o) {

			fixed4 c = pow ((_Color * _AmbientColor),_MySlider);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
