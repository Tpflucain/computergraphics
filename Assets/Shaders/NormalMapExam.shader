﻿Shader "Custom/NormalMapExam" {
	Properties {
		_Color2 ("Color", Color) = (1,1,1,1)
		_NormalMap ("Normal map", 2D) = "bump" {}
		_NormalMapIntensity("Normal Intensity", Range (0,5)) = 0.5
		_ScrollXSpeed("X scroll speed", Range(0,10)) = 2
		_ScrollYSpeed("Y scroll speed", Range(0,10)) = 0

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _NormalMap;
		float _NormalMapIntensity;
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;
		fixed4 _Color2;

		struct Input {
			float2 uv_NormalMap;
			float2 uv_MainTex;

		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed2 scrolledUV = IN.uv_NormalMap;
			fixed xScrollValue = _ScrollXSpeed * _Time;
			fixed yScrollValue = _ScrollYSpeed * _Time;
			scrolledUV += fixed2(xScrollValue, yScrollValue);
			o.Albedo =_Color2.rgb;
			float3 normalMap = UnpackNormal(tex2D(_NormalMap, scrolledUV));
			normalMap.x *= _NormalMapIntensity;
			normalMap.y *= _NormalMapIntensity;
			o.Normal = normalize(normalMap);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
