﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingPong1 : MonoBehaviour {

	Renderer rend;

	// Use this for initialization
	void Start () {

		rend = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		float ambient = Mathf.PingPong (Time.time, 5);
		rend.material.SetFloat ("_MySlider", ambient);
		rend.material.SetFloat ("_NormalMapIntensity", ambient);
	}
}
