﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Luz : MonoBehaviour {

	public float contador = 0;

	// Use this for initialization
	void Start () {

		this.GetComponent<Light> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		contador += Time.deltaTime;
		if (contador >= 10)
			this.GetComponent<Light> ().enabled = true;
	}
}
